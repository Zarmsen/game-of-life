package de.harms.gameoflife;


import de.harms.gameoflife.core.Activity;
import lombok.NoArgsConstructor;

/**
 * The {@link Application}.
 */
@NoArgsConstructor
public final class Application {


  /**
   * The entry point.
   *
   * @param args the args.
   */
  public static void main(final String[] args) {

    Activity.setup()
        .run();
  }
}
