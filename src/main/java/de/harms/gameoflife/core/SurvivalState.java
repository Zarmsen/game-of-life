package de.harms.gameoflife.core;

import java.util.Arrays;
import java.util.Optional;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The state of survival of a cell.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SurvivalState {

  /**
   * The cell is alive.
   */
  LIVE('*'),

  /**
   * The cell is dead.
   */
  DEAD('.');

  /**
   * Gets the output value of the state.
   */
  @Getter
  private final char outputValue;

  /**
   * Parse the output value character to {@link SurvivalState}.
   *
   * @param value The value.
   * @return the optional {@link SurvivalState}.
   */
  public static Optional<SurvivalState> parseCharacter(char value) {

    return Arrays.stream(SurvivalState.values())
        .filter(status -> status.getOutputValue() == value)
        .findFirst();

  }
}
