package de.harms.gameoflife.core;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * Represents a cell.
 */
@AllArgsConstructor
@EqualsAndHashCode
public final class Cell implements Comparable<Cell> {

  /**
   * The position of the cell.
   */
  @Getter
  private final CellPosition position;

  /**
   * The state of the cell.
   */
  @Getter
  @Setter
  private SurvivalState state;

  /**
   * The count of the alive neighbors.
   */
  @Getter
  @Setter
  private int aliveNeighbors;

  /**
   * Create a cell without the alive neighbors.
   *
   * @param position The position.
   * @param state    the state.
   * @return new instance of {@link Cell},
   */
  public static Cell of(CellPosition position, SurvivalState state) {
    return new Cell(position, state, 0);
  }


  @Override
  public String toString() {
    return String.format("%s %s %d", this.position.toString(), this.state.getOutputValue(),
        this.aliveNeighbors);
  }

  @Override
  public int compareTo(Cell o) {
    return this.position.compareTo(o.position);
  }
}
