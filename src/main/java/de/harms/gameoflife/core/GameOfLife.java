package de.harms.gameoflife.core;

import de.harms.gameoflife.core.print.GridPrinter;
import de.harms.gameoflife.core.rule.Engine;
import lombok.NonNull;
import lombok.SneakyThrows;

/**
 * The game of live.
 */
public final class GameOfLife implements Game {

  private final GameSettings settings;

  private final Engine engine;

  private final GridPrinter printer;

  /**
   * Initialize the instance.
   *
   * @param settings The settings of the grid.
   * @param engine     the engine.
   * @param printer    The printer.
   */
  public GameOfLife(@NonNull GameSettings settings,
      @NonNull Engine engine,
      @NonNull GridPrinter printer) {

    this.settings = settings;
    this.engine = engine;
    this.printer = printer;
  }

  /**
   * Start the game.
   */
  @Override
  @SneakyThrows
  public void run() {

    var builder = this.settings.getBuilder();
    var grid = builder.build();

    this.printer.print(grid);

    for (var round = 1; round < this.settings.getGenerations(); round++) {

      this.engine.apply(grid);
      this.printer.print(grid);
    }
  }
}
