package de.harms.gameoflife.core;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * The generation of the grid.
 */
@EqualsAndHashCode
public final class Generation {


  /**
   * The number of the generation.
   */
  @Getter
  private int number;

  /**
   * Prevent initialization.
   */
  private Generation() {
    this.number = 1;
  }

  /**
   * Initialize the generation with number 1.
   *
   * @return the generation.
   */
  public static Generation init() {
    return new Generation();
  }


  @Override
  public String toString() {
    return "Generation: " + this.number;
  }

  /**
   * Defines the next generation.
   */
  public void next() {
    this.number++;
  }
}
