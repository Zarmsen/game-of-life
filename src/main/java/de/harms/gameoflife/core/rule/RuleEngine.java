package de.harms.gameoflife.core.rule;

import de.harms.gameoflife.core.Cell;
import de.harms.gameoflife.core.Grid;
import de.harms.gameoflife.core.SurvivalState;
import java.util.List;
import lombok.NonNull;

/**
 * The rule engine to check the cell.
 */
public final class RuleEngine implements Engine {

  private static final List<Rule> RULES = List.of(
      new LiveFewerTwoNeighboursDiesRule(),
      new LiveMoreThreeNeighboursDiesRule(),
      new LiveMoreThreeNeighboursDiesRule(),
      new DeadWithThreeNeighboursLiveRule());


  @Override
  public void apply(@NonNull Grid grid) {

    setAliveNeighbors(grid);
    setSurvivalState(grid);
    grid.getGeneration().next();
  }

  /**
   * Set the alive neighbors of the cells.
   *
   * @param grid the grid.
   */
  private void setAliveNeighbors(Grid grid) {

    var tempCells = grid.getCells();

    for (var cell : grid.getCells()) {

      var neighbors = cell.getPosition().buildNeighbors();

      var aliveNeighborsCount = Math.toIntExact(
          tempCells.stream().filter(x -> neighbors.contains(x.getPosition()))
              .filter(x -> x.getState() == SurvivalState.LIVE)
              .count());

      cell.setAliveNeighbors(aliveNeighborsCount);
    }
  }

  /**
   * Set the survival state of all the cells in the grid.
   *
   * @param grid The grid.
   */
  private void setSurvivalState(Grid grid) {

    grid.getCells().forEach(x -> x.setState(applyRules(x)));

  }

  /**
   * Apply the rules for the cell.
   *
   * @param cell The cell.
   * @return the state.
   */
  private SurvivalState applyRules(Cell cell) {

    var result = cell.getState();

    for (var rule : RULES) {

      var newState = rule.execute(cell);
      if (newState != result) {
        result = newState;
        break;
      }
    }

    return result;
  }
}
