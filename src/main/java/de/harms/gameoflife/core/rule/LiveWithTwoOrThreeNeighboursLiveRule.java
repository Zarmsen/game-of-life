package de.harms.gameoflife.core.rule;

import de.harms.gameoflife.core.Cell;
import de.harms.gameoflife.core.SurvivalState;

/**
 * 3. Any live cell with two or three live neighbours lives on to the next generation.
 */
class LiveWithTwoOrThreeNeighboursLiveRule implements Rule {

  @Override
  public SurvivalState execute(Cell cell) {

    return cell.getState();
  }
}
