package de.harms.gameoflife.core.rule;

import de.harms.gameoflife.core.Grid;
import lombok.NonNull;

/**
 * Defines the Engine.
 */
public interface Engine {

  /**
   * Apply the engine on the grid.
   *
   * @param grid The grid.
   */
  void apply(@NonNull Grid grid);

}
