package de.harms.gameoflife.core.rule;

import de.harms.gameoflife.core.Cell;
import de.harms.gameoflife.core.SurvivalState;

/**
 * Rule 1: Any live cell with fewer than two live neighbours dies, as if caused by underpopulation.
 */
class LiveFewerTwoNeighboursDiesRule implements Rule {

  private static final int RULE_NEIGHBOURS = 2;

  @Override
  public SurvivalState execute(Cell cell) {

    var isAlive = SurvivalState.LIVE == cell.getState();

    if (isAlive) {
      return cell.getAliveNeighbors() < RULE_NEIGHBOURS
          ? SurvivalState.DEAD
          : cell.getState();
    }

    return cell.getState();

  }
}
