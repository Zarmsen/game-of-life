package de.harms.gameoflife.core.rule;

import de.harms.gameoflife.core.Cell;
import de.harms.gameoflife.core.SurvivalState;

/**
 * Definiert eine Regel zum ermitteln des aktuellen Position.
 */
public interface Rule {

  /**
   * Execute the rule.
   *
   * @param cell the cell.
   * @return the new {@link SurvivalState}.
   */
  SurvivalState execute(Cell cell);

}
