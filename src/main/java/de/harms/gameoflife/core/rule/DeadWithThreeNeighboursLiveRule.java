package de.harms.gameoflife.core.rule;

import de.harms.gameoflife.core.Cell;
import de.harms.gameoflife.core.SurvivalState;

/**
 * The rule: 4. Any dead cell with exactly three live neighbours becomes a live cell.
 */
class DeadWithThreeNeighboursLiveRule implements Rule {

  private static final int RULE_NEIGHBOURS = 3;

  @Override
  public SurvivalState execute(Cell cell) {

    var isDead = SurvivalState.DEAD == cell.getState();

    if (isDead) {
      return cell.getAliveNeighbors() == RULE_NEIGHBOURS
          ? SurvivalState.LIVE
          : cell.getState();
    }

    return cell.getState();

  }
}
