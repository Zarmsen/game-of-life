package de.harms.gameoflife.core.rule;

import de.harms.gameoflife.core.Cell;
import de.harms.gameoflife.core.SurvivalState;

/**
 * 2. Any live cell with more than three live neighbours dies, as if by overcrowding.
 */
class LiveMoreThreeNeighboursDiesRule implements Rule {

  private static final int RULE_NEIGHBOURS = 3;

  @Override
  public SurvivalState execute(Cell cell) {

    var isAlive = SurvivalState.LIVE == cell.getState();

    if (isAlive) {
      return cell.getAliveNeighbors() > RULE_NEIGHBOURS
          ? SurvivalState.DEAD
          : cell.getState();
    }

    return cell.getState();
  }
}
