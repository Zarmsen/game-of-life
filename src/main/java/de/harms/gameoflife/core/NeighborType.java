package de.harms.gameoflife.core;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Represents the types of neighboring cells.
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum NeighborType {

  /**
   * The top of the neighbor cell.
   */
  TOP(0, 1),

  /**
   * The top right position of the neighbor cell.
   */
  TOP_RIGHT(1, 1),

  /**
   * The right position of the neighbor cell.
   */
  RIGHT(1, 0),

  /**
   * The right bottom position of the neighbor cell.
   */
  RIGHT_BOTTOM(1, -1),

  /**
   * The bottom position of the neighbor cell.
   */
  BOTTOM(0, -1),

  /**
   * The bottom left position of the neighbor cell.
   */
  BOTTOM_LEFT(-1, -1),

  /**
   * The left position of the neighbor cell.
   */
  LEFT(-1, 0),

  /**
   * The left top of the neighbor cell.
   */
  LEFT_TOP(-1, 1);

  /**
   * The x position of the neighbor cell.
   */
  private final int x;

  /**
   * The x position of the neighbor cell.
   */
  private final int y;

}
