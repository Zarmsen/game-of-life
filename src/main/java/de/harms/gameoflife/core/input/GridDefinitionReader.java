package de.harms.gameoflife.core.input;

import de.harms.gameoflife.core.GridDefinition;
import java.io.File;
import lombok.NonNull;

/**
 * The reader to {@link GridDefinition}.
 */
public interface GridDefinitionReader {

  /**
   * Read a grid file to create a {@link GridDefinition}.
   *
   * @param file The file.
   * @return The {@link GridDefinition}.
   */
  GridDefinition read(@NonNull File file);
}
