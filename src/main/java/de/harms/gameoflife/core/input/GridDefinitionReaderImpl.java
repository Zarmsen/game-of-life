package de.harms.gameoflife.core.input;

import de.harms.gameoflife.core.CellPosition;
import de.harms.gameoflife.core.GridDefinition;
import de.harms.gameoflife.core.SurvivalState;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

/**
 * The implementation of {@link GridDefinitionReader}.
 */
public class GridDefinitionReaderImpl implements
    GridDefinitionReader {

  @SneakyThrows
  @Override
  public GridDefinition read(@NonNull File file) {

    var notExists = !file.exists();
    if (notExists) {
      throw new IllegalArgumentException(file.getAbsolutePath() + " not exists.");
    }

    var lines = Files.readAllLines(Paths.get(file.getPath()),
        StandardCharsets.UTF_8);

    var alivePositions = new ArrayList<CellPosition>();
    var columns = 0;
    for (var x = 0; x < lines.size(); x++) {

      var row = StringUtils.trim(lines.get(x)).toCharArray();

      for (var y = 0; y < row.length; y++) {

        var stateCharacter = row[y];
        var state = SurvivalState.parseCharacter(stateCharacter);

        if (state.isEmpty()) {
          throw new IllegalArgumentException(
              String.format("Cannot read char '%s' in the file.", stateCharacter));
        }

        if (SurvivalState.LIVE == state.get()) {
          alivePositions.add(CellPosition.of(x, y));
        }
      }

      if (row.length > columns) {
        columns = row.length;
      }
    }

    return GridDefinition.of(lines.size(), columns, alivePositions);
  }
}
