package de.harms.gameoflife.core;

import java.util.HashSet;
import java.util.Objects;
import lombok.NonNull;

/**
 * The grid builder for the cells.
 */
public final class GridBuilder {

  private final GridDefinition definition;

  /**
   * Initialize the Instance.
   *
   * @param definition The definition of the grid. Cannot be null.
   */
  public GridBuilder(@NonNull GridDefinition definition) {
    this.definition = definition;
  }

  /**
   * Creates the grid based on the {@link Generation}.
   *
   * @return the grid.
   */
  public Grid build() {

    var result = new HashSet<Cell>();

    for (var x = 0; x < this.definition.getRows(); x++) {

      for (var y = 0; y < this.definition.getColumns(); y++) {

        var position = CellPosition.of(x, y);
        result.add(Cell.of(position, determineState(position)));

      }
    }
    return new Grid(result);
  }


  /**
   * Determines the {@link SurvivalState} based on the surviving cells.
   *
   * @param position the current position.
   * @return the determine survival state.
   */
  private SurvivalState determineState(@NonNull CellPosition position) {

    return this.definition.getAlivePositions().stream().anyMatch(x -> Objects.equals(x, position))
        ? SurvivalState.LIVE
        : SurvivalState.DEAD;
  }
}
