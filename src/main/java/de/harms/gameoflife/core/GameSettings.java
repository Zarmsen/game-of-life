package de.harms.gameoflife.core;

import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

/**
 * The game of live definition.
 */
@AllArgsConstructor
@EqualsAndHashCode
public class GameSettings {

  /**
   * Count of rounds for generation.
   */
  @Getter
  private final int generations;
  /**
   * The definition of the grid.
   */
  @Getter
  private final GridDefinition definition;
  /**
   * The builder for the grid.
   */
  private GridBuilder builder;

  /**
   * Initialize the instance.
   *
   * @param generations The count of generations.
   * @param definition  The definition of the grid. Cannot be null.
   */
  private GameSettings(int generations, @NonNull GridDefinition definition) {

    this.generations = generations;
    this.definition = definition;
  }

  /**
   * Create new instance.
   *
   * @param generations The count of the generation.
   * @param definition  The definition of the grid. Cannot be null.
   * @return instance of {@link GameSettings},
   */
  public static GameSettings of(int generations, GridDefinition definition) {
    return new GameSettings(generations, definition);
  }

  /**
   * Returns the grid builder.
   *
   * @return the {@link GridBuilder}.
   */
  public GridBuilder getBuilder() {

    if (Objects.isNull(this.builder)) {
      this.builder = new GridBuilder(this.definition);
    }

    return this.builder;
  }
}
