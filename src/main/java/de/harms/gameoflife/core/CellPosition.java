package de.harms.gameoflife.core;

import java.util.HashSet;
import java.util.Set;
import lombok.Value;

/**
 * Represents the position of a cell.
 */
@Value(staticConstructor = "of")
public class CellPosition implements Comparable<CellPosition> {

  /**
   * The x position of the cell.
   */
  private final int x;

  /**
   * The y position of the cell.
   */
  private final int y;

  /**
   * Build the neighbors of the {@link CellPosition}.
   *
   * @return list of {@link CellPosition}s.
   */
  public Set<CellPosition> buildNeighbors() {

    var neighbors = NeighborType.values();
    var result = new HashSet<CellPosition>();

    for (var neighbor : neighbors) {

      var newX = this.x + neighbor.getX();
      var newY = this.y + neighbor.getY();

      result.add(CellPosition.of(newX, newY));

    }
    return result;
  }

  @Override
  public String toString() {
    return String.format("x: %d | y: %d", this.x, this.y);
  }

  @Override
  public int compareTo(CellPosition o) {

    if (this.x == o.x) {
      if (this.y == o.y) {
        return 0;
      }
      return this.y > o.y ? 1 : -1;
    }
    return this.x > o.x ? 1 : -1;
  }
}
