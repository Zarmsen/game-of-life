package de.harms.gameoflife.core;


import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.NonNull;

/**
 * Represents the grid.
 */
public class Grid {

  @Getter
  private final Generation generation = Generation.init();

  private final Set<Cell> cells;

  /**
   * Initialize the instance.
   *
   * @param cells the cells of the grid. Cannot be null.
   */
  public Grid(@NonNull Set<Cell> cells) {

    this.cells = new HashSet<>(cells);
  }

  /**
   * Return the cells of the grid.
   *
   * @return the cells.
   */
  public Set<Cell> getCells() {
    return this.cells;
  }

  /**
   * Gets the alive cells.
   *
   * @return the cell.
   */
  public List<CellPosition> getAlivePositions() {
    return this.cells.stream()
        .filter(x -> x.getState() == SurvivalState.LIVE)
        .map(Cell::getPosition)
        .sorted()
        .collect(Collectors.toList());
  }
}
