package de.harms.gameoflife.core;

import de.harms.gameoflife.core.input.GridDefinitionReaderImpl;
import de.harms.gameoflife.core.print.ConsoleGridPrinter;
import de.harms.gameoflife.core.print.ConsoleOutput;
import de.harms.gameoflife.core.print.GridPrinter;
import de.harms.gameoflife.core.rule.Engine;
import de.harms.gameoflife.core.rule.RuleEngine;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

/**
 * The fluent api to start the activity of Game of life.
 */
public final class Activity {

  private static final List<CellPosition> DEFAULT_ALIVE_CELLS = List.of(
      CellPosition.of(0, 1),
      CellPosition.of(0, 5),
      CellPosition.of(1, 3),
      CellPosition.of(2, 1),
      CellPosition.of(3, 3),
      CellPosition.of(4, 1));
  private final Engine engine;
  private final GridPrinter gridPrinter;
  private GameSettings settings;

  /**
   * Prevent to initialize the instance and set the default values.
   */
  private Activity() {

    this.settings = GameSettings.of(2, GridDefinition.of(5, 8,
        DEFAULT_ALIVE_CELLS));
    this.engine = new RuleEngine();
    this.gridPrinter = new ConsoleGridPrinter(new ConsoleOutput());

  }

  /**
   * @param settings    The settings.
   * @param engine      the engine.
   * @param gridPrinter The printer.
   */
  private Activity(GameSettings settings, Engine engine,
      GridPrinter gridPrinter) {
    this.settings = settings;
    this.engine = engine;
    this.gridPrinter = gridPrinter;
  }

  /**
   * Returns the instance.
   *
   * @return Instance on {@link Activity}
   */
  public static Activity setup() {

    return InstanceHolder.instance;
  }

  /**
   * Run the game.
   */
  @SuppressWarnings("squid:S106")
  public void run() {
    var gameOfLife = new GameOfLife(this.settings, this.engine, this.gridPrinter);
    gameOfLife.run();

    var scanner = new Scanner(System.in);
    System.out.print("Press any key to continue . . . ");
    scanner.nextLine();
  }

  /**
   * Defines the generation for the game.
   *
   * @param generations The generations of the game of live game play.
   * @return the {@link Activity}.
   */
  public Activity generations(int generations) {

    this.settings = GameSettings.of(generations,
        this.settings.getDefinition());
    return new Activity(this.settings, this.engine, this.gridPrinter);

  }

  /**
   * Defines the {@link GridDefinition} by file.
   *
   * @param file The import file.
   * @return the Activity.
   */
  public Activity file(String file) {

    var fileReader = new GridDefinitionReaderImpl();
    var gridDefinition = fileReader.read(buildFile(file));
    this.settings = GameSettings.of(this.settings.getGenerations(),
        gridDefinition);
    return new Activity(this.settings, this.engine, this.gridPrinter);

  }

  /**
   * Defines the {@link Grid} by definition.
   *
   * @param rows    The max count of rows in the grid.
   * @param columns The max columns of the grid.
   * @return the Activity.
   */
  public Activity grid(int rows, int columns) {

    var gridDefinition = GridDefinition.of(rows, columns,
        this.settings.getDefinition().getAlivePositions());
    this.settings = GameSettings.of(this.settings.getGenerations(),
        gridDefinition);
    return new Activity(this.settings, this.engine, this.gridPrinter);
  }

  /**
   * Defines the alive cells in the {@link Grid} for the definition.
   *
   * @param cells the live cells of the grid
   * @return the Activity.
   */
  public Activity alive(CellPosition... cells) {

    var gridDefinition = GridDefinition.of(this.settings.getDefinition().getRows(),
        this.settings.getDefinition().getColumns(),
        Arrays.asList(cells));
    this.settings = GameSettings.of(this.settings.getGenerations(),
        gridDefinition);
    return new Activity(this.settings, this.engine, this.gridPrinter);
  }

  /**
   * Defines the random alive cells in the {@link Grid} for the definition.
   *
   * @param count the random count of the  live cells of the grid.
   * @return the Activity.
   */
  public Activity aliveRandom(int count) {

    var cells = new HashSet<CellPosition>();

    var random = new Random();

    while (cells.size() < count) {
      cells.add(CellPosition.of(random.nextInt(this.settings.getDefinition().getRows() - 1),
          random.nextInt(this.settings.getDefinition().getColumns() - 1)));
    }

    var gridDefinition = GridDefinition.of(this.settings.getDefinition().getRows(),
        this.settings.getDefinition().getColumns(),
        new ArrayList<>(cells));
    this.settings = GameSettings.of(this.settings.getGenerations(),
        gridDefinition);
    return new Activity(this.settings, this.engine, this.gridPrinter);

  }

  /**
   * Build the file from classpath.
   *
   * @param file The file.
   * @return content
   */
  private File buildFile(String file) {

    var classLoader = getClass().getClassLoader();

    return new File(Objects.requireNonNull(
        classLoader.getResource(file)).getFile());
  }

  /**
   * InstanceHolder.
   */
  private static final class InstanceHolder {

    /**
     * Instance.
     */
    private static final Activity instance = new Activity();
  }

}
