package de.harms.gameoflife.core;

/**
 * The game of life.
 */
public interface Game {

  /**
   * Start the game.
   */
  void run();
}
