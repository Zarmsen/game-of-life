package de.harms.gameoflife.core.print;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

/**
 * Implements {@link Output} for the console Output.
 */
@NoArgsConstructor
@SuppressWarnings("squid:S106")
public final class ConsoleOutput implements Output {

  @Override
  public void print(final String line) {
    System.out.println(line);
  }
}
