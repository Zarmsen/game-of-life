package de.harms.gameoflife.core.print;

/**
 * Is responsible for the output.
 */
public interface Output {

  /**
   * Print a line.
   *
   * @param line a line
   */
  void print(String line);
}
