package de.harms.gameoflife.core.print;

import de.harms.gameoflife.core.Grid;
import lombok.NonNull;

/**
 * The printer for the {@link Grid}.
 */
public interface GridPrinter {

  /**
   * Print the {@link Grid}.
   *
   * @param grid The grid.
   */
  void print(@NonNull Grid grid);
}
