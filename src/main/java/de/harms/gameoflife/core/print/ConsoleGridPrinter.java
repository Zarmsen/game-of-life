package de.harms.gameoflife.core.print;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toSet;

import de.harms.gameoflife.core.Grid;
import java.util.stream.Collectors;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;

/**
 * The printer for the grid.
 */
@AllArgsConstructor
public class ConsoleGridPrinter implements GridPrinter {

  private final Output output;

  @Override
  public void print(@NonNull Grid grid) {

    this.output.print(grid.getGeneration().toString());
    this.output.print(StringUtils.EMPTY);

    var rows = grid.getCells()
        .stream()
        .sorted()
        .collect(groupingBy(x -> x.getPosition().getX(), toSet()));

    for (var row : rows.entrySet()) {

      var characters = row.getValue()
          .stream()
          .sorted()
          .map(x -> String.valueOf(x.getState().getOutputValue()))
          .collect(Collectors.toList());

      var line = String.join(StringUtils.EMPTY, characters);
      this.output.print(line);
    }

    var count = grid.getCells().size();
    var aliveCount = grid.getAlivePositions().size();
    this.output.print(String.format("LIVE: %d DEAD: %d", aliveCount, count - aliveCount));
    this.output.print(StringUtils.EMPTY);

  }
}
