package de.harms.gameoflife.core;

import java.util.List;
import lombok.NonNull;
import lombok.Value;

/**
 * The value of the grid definition.
 */
@Value(staticConstructor = "of")
public class GridDefinition {

  /**
   * The line-wise definition and stands for the maximum X in the coordinate system.
   */
  private final int rows;


  /**
   * The column-wise definition and stands for the maximum Y in the coordinate system.
   */
  private final int columns;


  /**
   * Define the alive positions.
   */
  @NonNull
  private final List<CellPosition> alivePositions;


  @Override
  public String toString() {
    return this.rows + " " + this.columns;
  }
}
