package de.harms.gameoflife.core.rule;

import static org.junit.jupiter.api.Assertions.assertEquals;

import de.harms.gameoflife.core.Cell;
import de.harms.gameoflife.core.CellPosition;
import de.harms.gameoflife.core.SurvivalState;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


@DisplayName("Rule: Any dead cell with exactly three live neighbours becomes a live cell")
class DeadWithThreeNeighboursLiveRuleTest {

  @DisplayName("should check rule")
  @ParameterizedTest
  @CsvSource({
      "LIVE, 1, LIVE",
      "LIVE, 2, LIVE",
      "LIVE, 3, LIVE",
      "LIVE, 4, LIVE",
      "DEAD, 1, DEAD",
      "DEAD, 2, DEAD",
      "DEAD, 3, LIVE",
      "DEAD, 4, DEAD"})
  void should_check_rule(SurvivalState current, int aliveNeighboursCount, SurvivalState expected) {

    var sut = new DeadWithThreeNeighboursLiveRule();
    var position = CellPosition.of(3, 5);
    var actual = sut.execute(new Cell(position, current, aliveNeighboursCount));

    assertEquals(expected, actual);
  }
}
