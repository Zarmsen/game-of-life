package de.harms.gameoflife.core.rule;

import static org.junit.jupiter.api.Assertions.assertEquals;

import de.harms.gameoflife.core.CellPosition;
import de.harms.gameoflife.core.GridBuilder;
import de.harms.gameoflife.core.GridDefinition;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


@DisplayName("Rule Engine")
class RuleEngineTest {

  @DisplayName("should check the rules of every cell in the grid")
  @Test
  void should_check_the_rules_of_every_cell_in_the_grid() {

    var aliveCells = List.of(
        CellPosition.of(0, 2),
        CellPosition.of(0, 6),
        CellPosition.of(1, 4),
        CellPosition.of(2, 2),
        CellPosition.of(3, 4),
        CellPosition.of(4, 2));
    var builder = new GridBuilder(GridDefinition.of(5, 8, aliveCells));
    var grid = builder.build();

    var sut = new RuleEngine();
    sut.apply(grid);

    var expected = List.of(
        CellPosition.of(1, 3),
        CellPosition.of(2, 3),
        CellPosition.of(3, 3));

    assertEquals(2, grid.getGeneration().getNumber());
    assertEquals(expected, grid.getAlivePositions());
  }
}
