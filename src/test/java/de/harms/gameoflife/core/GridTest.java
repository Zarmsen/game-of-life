package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


@DisplayName("Grid")
class GridTest {

  @DisplayName("build cells should create cells")
  @Test
  void build_cells_should_create_cells() {

    var alivePosition = List.of(CellPosition.of(2, 3));
    var builder = new GridBuilder(GridDefinition.of(5, 8, alivePosition));

    var cells = builder.build().getCells();
    var actual = new Grid(cells);

    assertEquals(cells, actual.getCells());
    assertEquals(alivePosition, IterableUtils.toList(actual.getAlivePositions()));
  }

  @DisplayName("should fail if cells null")
  @Test
  void should_fail_if_cells_null() {

    //noinspection ConstantConditions
    assertThrows(IllegalArgumentException.class, () -> new Grid(null));
  }
}

