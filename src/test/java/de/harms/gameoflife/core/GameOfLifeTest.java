package de.harms.gameoflife.core;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

import de.harms.gameoflife.core.print.GridPrinter;
import de.harms.gameoflife.core.print.Output;
import de.harms.gameoflife.core.rule.Engine;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Console Grid Printer")
class GameOfLifeTest {

  @Mock
  private Output output;

  @Mock
  private Engine engine;

  @Mock
  private GridPrinter printer;

  private Game sut;

  /**
   * Setup.
   */
  @BeforeEach
  void setUp() {

    Assertions.assertNotNull(this.output, "output service not set");

    var aliveCells = List.of(
        CellPosition.of(0, 1),
        CellPosition.of(0, 5),
        CellPosition.of(1, 3),
        CellPosition.of(2, 1),
        CellPosition.of(3, 3),
        CellPosition.of(4, 1));

    var settings = GameSettings.of(2, GridDefinition.of(5, 8, aliveCells));
    this.sut = new GameOfLife(settings, this.engine, printer);
  }

  @DisplayName("should run")
  @Test
  void should_run() {

    this.sut.run();

    verify(this.engine, atLeast(1)).apply(any());
    verify(this.printer, atLeast(2)).print(any());
  }
}

