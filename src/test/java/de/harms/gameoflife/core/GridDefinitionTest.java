package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Grid Definition")
class GridDefinitionTest {

  @DisplayName("should create a value")
  @Test
  void should_create_a_value() {

    var rows = 5;
    var columns = 8;
    var alivePositions = List.of(CellPosition.of(3, 4));

    var toString = "5 8";

    var actual = GridDefinition.of(rows, columns, alivePositions);

    assertEquals(rows, actual.getRows());
    assertEquals(columns, actual.getColumns());
    assertEquals(toString, actual.toString());
    assertEquals(alivePositions, actual.getAlivePositions());
    assertNotEquals(0, actual.hashCode());
  }
}
