package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@DisplayName("Survival State")
class SurvivalStateTest {

  @DisplayName("should parse")
  @ParameterizedTest
  @CsvSource({
      "LIVE, LIVE, *",
      "DEAD, DEAD, ."})
  void should_parse(String name, SurvivalState expected, char output) {

    var actual = SurvivalState.valueOf(name);

    assertEquals(expected, actual);
    assertEquals(output, actual.getOutputValue());


  }

  @DisplayName("should parse character")
  @ParameterizedTest
  @CsvSource({
      "LIVE, *",
      "DEAD, ."})
  void should_parse_character(SurvivalState expected, char value) {

    var actual = SurvivalState.parseCharacter(value).orElse(null);

    assertEquals(expected, actual);


  }
}
