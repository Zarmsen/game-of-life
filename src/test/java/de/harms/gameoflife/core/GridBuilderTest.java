package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Grid Builder")
class GridBuilderTest {

  @DisplayName("build cells should create cells")
  @Test
  void build_cells_should_create_cells() {

    var alivePosition = List.of(CellPosition.of(2, 3));
    var builder = new GridBuilder(GridDefinition.of(5, 8, alivePosition));

    var grid = builder.build();

    var aliveCells = IterableUtils.filteredIterable(grid.getCells(),
        (x) -> SurvivalState.LIVE == x.getState());

    assertEquals(40, IterableUtils.size(grid.getCells()));
    assertEquals(1, IterableUtils.size(aliveCells));
  }


  @DisplayName("should fail if definition null")
  @Test
  void should_fail_if_definition_null() {

    //noinspection ConstantConditions
    assertThrows(IllegalArgumentException.class, () -> new GridBuilder(null));
  }
}
