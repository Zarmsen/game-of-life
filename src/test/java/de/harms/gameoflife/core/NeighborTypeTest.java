package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


@DisplayName("Neighbor Type")
class NeighborTypeTest {

  @DisplayName("should parse")
  @ParameterizedTest
  @CsvSource({
      "TOP, TOP, 0, 1",
      "TOP_RIGHT, TOP_RIGHT, 1, 1",
      "RIGHT, RIGHT, 1, 0",
      "RIGHT_BOTTOM, RIGHT_BOTTOM, 1, -1",
      "BOTTOM, BOTTOM, 0, -1",
      "BOTTOM_LEFT, BOTTOM_LEFT, -1, -1",
      "LEFT, LEFT, -1, 0",
      "LEFT_TOP, LEFT_TOP, -1, 0"})
  void should_parse(String name, NeighborType expected, int x, int y) {

    var actual = NeighborType.valueOf(name);

    assertEquals(expected, actual);

  }
}
