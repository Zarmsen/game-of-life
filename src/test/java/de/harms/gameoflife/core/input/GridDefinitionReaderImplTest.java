package de.harms.gameoflife.core.input;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import de.harms.gameoflife.core.CellPosition;
import de.harms.gameoflife.core.GridDefinition;
import java.io.File;
import java.util.List;
import java.util.Objects;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


@DisplayName("Grid Definition Reader")
class GridDefinitionReaderImplTest {

  private GridDefinitionReader sut;

  @BeforeEach
  void setUp() {

    this.sut = new GridDefinitionReaderImpl();
  }

  @Test
  @DisplayName("should fail if file not exists")
  void should_fail_if_file_not_exits() {

    var file = new File("");
    assertThrows(IllegalArgumentException.class, () -> this.sut.read(file));
  }

  @SneakyThrows
  @Test
  @DisplayName("should fail if file null")
  void should_fail_if_file_null() {

    //noinspection ConstantConditions
    assertThrows(IllegalArgumentException.class, () -> this.sut.read(null));
  }


  @DisplayName("should read the file to return the grid definition")
  @Test
  void should_check_the_rules_of_every_cell_in_the_grid() {

    var aliveCells = List.of(
        CellPosition.of(0, 1),
        CellPosition.of(0, 5),
        CellPosition.of(1, 3),
        CellPosition.of(2, 1),
        CellPosition.of(3, 3),
        CellPosition.of(4, 1));
    var expected = GridDefinition.of(5, 8, aliveCells);

    var actual = this.sut.read(buildFile());

    assertEquals(expected, actual);
  }

  /**
   * Build the file from classpath.
   *
   * @return content
   */
  private File buildFile() {

    var classLoader = getClass().getClassLoader();

    return new File(Objects.requireNonNull(
        classLoader.getResource("game-of-life.txt")).getFile());
  }
}
