package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Cell Position")
class CellPositionTest {

  @DisplayName("should create a value")
  @Test
  void should_create_a_value() {

    var x = -1;
    var y = 9;
    var toString = "x: -1 | y: 9";

    var actual = CellPosition.of(x, y);

    assertEquals(x, actual.getX());
    assertEquals(y, actual.getY());
    assertEquals(toString, actual.toString());
    assertNotEquals(0, actual.hashCode());
  }

  @DisplayName("should build the neighbors")
  @Test
  void should_build_the_neighbors() {

    var expected = List.of(
        CellPosition.of(5, 2),
        CellPosition.of(5, 4),
        CellPosition.of(4, 2),
        CellPosition.of(4, 3),
        CellPosition.of(4, 4),
        CellPosition.of(6, 2),
        CellPosition.of(6, 3),
        CellPosition.of(6, 4));

    var position = CellPosition.of(5, 3);

    var actual = position.buildNeighbors();
    assertEquals(expected, IterableUtils.toList(actual));
  }

  @DisplayName("should be sortable by comparable")
  @Test
  void should_be_sortable_by_comparable() {

    var expected = List.of(
        CellPosition.of(0, 0),
        CellPosition.of(0, 0),
        CellPosition.of(0, 1),
        CellPosition.of(0, 2),
        CellPosition.of(1, 0),
        CellPosition.of(1, 1),
        CellPosition.of(1, 2));

    var unsorted = List.of(
        CellPosition.of(1, 0),
        CellPosition.of(1, 1),
        CellPosition.of(0, 0),
        CellPosition.of(0, 0),
        CellPosition.of(0, 2),
        CellPosition.of(0, 1),
        CellPosition.of(1, 2));

    var actual = unsorted.stream().sorted().collect(Collectors.toList());

    assertEquals(expected, actual);
  }


}
