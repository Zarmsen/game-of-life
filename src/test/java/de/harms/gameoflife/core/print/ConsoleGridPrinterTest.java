package de.harms.gameoflife.core.print;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;

import de.harms.gameoflife.core.CellPosition;
import de.harms.gameoflife.core.Grid;
import de.harms.gameoflife.core.GridBuilder;
import de.harms.gameoflife.core.GridDefinition;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
@DisplayName("Console Grid Printer")
class ConsoleGridPrinterTest {

  @Mock
  private Output output;

  private GridPrinter sut;

  /**
   * Setup.
   */
  @BeforeEach
  void setUp() {

    Assertions.assertNotNull(this.output, "output service not set");

    this.sut = new ConsoleGridPrinter(this.output);
  }

  @DisplayName("should_print_the_grid")
  @Test
  void should_print_the_grid() {

    var alivePositions = List.of(CellPosition.of(2, 3));
    var builder = new GridBuilder(GridDefinition.of(5, 8, alivePositions));
    var cells = builder.build().getCells();
    var grid = new Grid(cells);

    this.sut.print(grid);

    verify(this.output).print("Generation: 1");
    verify(this.output, atLeast(4)).print("........");
    verify(this.output, atLeast(1)).print("...*....");
    verify(this.output).print("LIVE: 1 DEAD: 39");
    verify(this.output, atLeast(2)).print(StringUtils.EMPTY);
  }

  @DisplayName("print should fail if grid null")
  @Test
  void print_should_fail_if_grid_null() {

    //noinspection ConstantConditions
    assertThrows(IllegalArgumentException.class, () -> this.sut.print(null));
  }
}

