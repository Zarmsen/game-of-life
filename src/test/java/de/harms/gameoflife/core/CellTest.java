package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Cell")
class CellTest {

  @DisplayName("should create a value")
  @Test
  void should_create_a_value() {

    var position = CellPosition.of(1, 3);
    var state = SurvivalState.LIVE;
    var aliveCount = 1;
    final var toString = position.toString() + " " + state.getOutputValue() + " " + aliveCount;

    var actual = new Cell(position, state, aliveCount);

    assertEquals(position, actual.getPosition());
    assertEquals(state, actual.getState());
    assertEquals(aliveCount, actual.getAliveNeighbors());
    assertEquals(toString, actual.toString());
    assertNotEquals(0, actual.hashCode());
  }


  @DisplayName("should change the values")
  @Test
  void should_change_the_values() {

    var position = CellPosition.of(1, 3);

    var actual = Cell.of(position, SurvivalState.DEAD);

    var state = SurvivalState.LIVE;
    var aliveCount = 1;

    actual.setState(state);
    actual.setAliveNeighbors(aliveCount);

    assertEquals(position, actual.getPosition());
    assertEquals(state, actual.getState());
    assertEquals(aliveCount, actual.getAliveNeighbors());
    assertNotEquals(0, actual.hashCode());
  }


  @DisplayName("should change the state")
  @Test
  void should_change_the_state() {

    var position = CellPosition.of(1, 3);
    var state = SurvivalState.LIVE;

    var actual = Cell.of(position, state);

    assertEquals(position, actual.getPosition());
    assertEquals(state, actual.getState());
    assertNotEquals(0, actual.hashCode());
  }


  @DisplayName("comparable should compare")
  @Test
  void comparable_should_compare() {

    var state = SurvivalState.LIVE;
    var aliveCount = 1;
    var first = new Cell(CellPosition.of(1, 3), state, aliveCount);
    var second = Cell.of(CellPosition.of(3, 1), state);

    assertEquals(-1, first.compareTo(second));
    assertEquals(1, second.compareTo(first));
    //noinspection EqualsWithItself
    assertEquals(0, first.compareTo(first));
  }

}
