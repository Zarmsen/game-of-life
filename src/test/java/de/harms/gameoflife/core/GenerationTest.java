package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


@DisplayName("Generation")
class GenerationTest {

  @DisplayName("should create a value")
  @Test
  void should_create_a_value() {

    var number = 1;
    var toString = "Generation: 1";

    var actual = Generation.init();

    assertEquals(number, actual.getNumber());
    assertEquals(toString, actual.toString());
    assertNotEquals(0, actual.hashCode());
  }


  @DisplayName("next should update the generation")
  @Test
  void next_should_update_the_generation() {

    var actual = Generation.init();

    actual.next();

    assertEquals(2, actual.getNumber());
  }
}
