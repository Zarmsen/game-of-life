package de.harms.gameoflife.core;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Game Settings Test")
class GameSettingsTest {

  @DisplayName("should create value")
  @Test
  void should_create_value() {

    var rounds = 3;
    var alivePosition = List.of(CellPosition.of(2, 3));
    var definition = GridDefinition.of(5, 8, alivePosition);

    var actual = GameSettings.of(rounds, definition);

    assertEquals(rounds, actual.getGenerations());
    assertEquals(definition, actual.getDefinition());
    assertNotNull(actual.getBuilder());
    assertFalse(actual.toString().isEmpty());
    assertNotEquals(0, actual.hashCode());
  }

  @DisplayName("should fail if definition null")
  @Test
  void should_fail_if_definition_null() {

    var rounds = 3;
    var alivePosition = List.of(CellPosition.of(2, 3));
    var definition = GridDefinition.of(5, 8, alivePosition);

    // noinspection ResultOfMethodCallIgnored
    assertThrows(IllegalArgumentException.class,
        () -> GameSettings.of(rounds, null));
  }
}
