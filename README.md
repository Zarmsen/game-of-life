# Game of life - Harms Kata
This Kata is about calculating the next generation of Conway’s game of life, given any
starting position. See the description [here](TASK.md).
---

## Frameworks

The following frameworks are used for support:

* [Apache Commons](http://commons.apache.org/)
* [Lombok](https://projectlombok.org/features/all)

---

## System requirements

* [Java 11](https://openjdk.java.net/)
* [Maven](http://maven.apache.org/)

---

## Run program

### Create package

``` bash
mvn clean package
```

### Execute Jar 

``` bash
java -jar target/game-of-life-0.0.1-SNAPSHOT.jar
```

---

## Fluent API

The `Activity` class is designed fluently and can be configured according to your needs or used in the `Application` class.
To create and run the `Activity`, the `run()` method must be executed.

### Standard

``` Java
Activity.setup().run();
```
* Output is via console
* The output contains 2 generations.
* The cells alive and the grid definitions are predefined from the [Test task](TASK.md).

### Configuration

``` Java

    Activity.setup()
        .file("game-of-life.txt")
        .generations(200)
        .grid(100, 200)
        .alive(CellPosition.of(2, 4), CellPosition.of(3, 4))
        .aliveRandom(10)
        .run();
```

* `file` - The start definitions can be loaded via a file. Grid size and alive cells are defined by it.
* `generations` - The number of generations can be specified. Default is 2.
* `grid` - The grid can be specified. Default are 5 rows and 8 columns.
* `alive` - The alive cells can be specified.
* `aliveRandom` - The specified alive cells will be created randomly.
* `run` - Starts the program

---

## ToDo's
* possibly create frontend with vaadin
* pass commandline arguments and evaluate [Commons CLI](http://commons.apache.org/proper/commons-cli/)
* output can still be defined for example via a text file
